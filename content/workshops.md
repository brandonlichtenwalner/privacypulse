---
title: "Workshops"
---

[Send us an email](/contact) to let us know which workshop or other topics interest you and where you would like to see them offered, and we'll notify you when we have an offering that matches. Or, [book a consultation](https://cal.com/privacypulse/10-min-discovery?user=privacypulse) if you're interested in hosting one of our workshops. 

## How Open Source Can Help Your Business
Our flagship hands-on workshop providing a quick overview of the what/why of open source and then diving into an audience-driven demo. We'll ask you to think about an application/service that you're unhappy with due to cost, privacy concerns, or other reasons--something an open source alternative is well positioned to solve--and come to a consensus on the item that attendees most want to replace. We'll dive in to a follow-along demonstration detailing the implementation of an open source solution to meet that business need before wrapping up with key takeaways and actionable follow-ups.

## What is Open Source
If you're asking yourself that question; we're here to answer it! This shorter workshop also aims to answer "Why should I use open source technologies?"

Topics include:
- The open source ethos
- Dispelling misconceptions about open source
- Potential benefits of open source solutions
- Avoiding pitfalls in your open source journey
- A brief introduction to "Giving Back"

## Giving Back: A Better Model for Open Source
This workshop provides actionable ideas  for being a good steward of the open source community. For open source to continue to thrive, the maintainers and developers need contributions of your time, talent, or treasure to keep the community thriving. After all, you're not (in most cases) paying for the software directly, so how can we give in other ways?

Topics include:
- Responsible bug filing
- Feature requests without entitlement
- A 90% discount you can feel good about
- Our pledge
